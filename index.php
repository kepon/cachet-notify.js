<?php

// cachet-notify.js

// Par David Mercereau
// Licence Beerware : https://fr.wikipedia.org/wiki/Beerware

header('Content-Type: application/javascript');
$config_brute=file_get_contents('config.json');
$config = json_decode($config_brute, true);
if ($config == NULL) {
	exit('Le fichier config.json retourne une erreur');
}

function getContentBycURL($strURL) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return data inplace of echoing on screen
	curl_setopt($ch, CURLOPT_URL, $strURL);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // Skip SSL Verification
	$rsData = curl_exec($ch);
	curl_close($ch);
	return $rsData;
}
if (!is_file($config['cacheFile'])) {
    $downloadNow=true;    
} else if (filemtime($config['cacheFile'])+$config['cacheTimeExpire'] < time()) {
    $downloadNow=true;
}
if ($downloadNow == true) {
    file_put_contents($config['cacheFile'], getContentBycURL($config['urlCachet']."/api/".$config['ApiCachetVersion']."/incidents"));
}

$cacheFile = file_get_contents($config['cacheFile']);
?>
var config = <?= $config_brute ?>;
var jsonFile = <?= $cacheFile ?>;

// End Configuration
function getUrlHash(){
    if( window.location.hash ){
        return window.location.hash.replace('#', '');
    }
    return false;
}

// onready : Inspiré : http://jsfiddle.net/electricvisions/Jacck/
document.onreadystatechange = function () {
    var state = document.readyState
    // A la fin du chargement du document Sauf si l'ancre cachet-notify-close est présente
    if (state == 'complete' && getUrlHash() != 'cachet-notify-close') {
        
        //console.log(jsonFile);
   
        var nbSticked = 0;
        var sticked = [];
        Object.entries(jsonFile.data).forEach(([ndata, data]) => {
            //console.log(ndata + ' ' + data.name + ' ' + data.stickied);
            if (data.stickied == true) {
                if (config.liste == true) {
                    sticked[nbSticked] = data;
                }
                nbSticked = nbSticked + 1;
            }
        });
        //console.log(sticked);
        if (nbSticked != 0) {
            var contenu = '';
            var closeBoutton = '<span id="close-cachet-notify" style="'+config.aCloseStyle+'"><a onClick="document.getElementById(\'cachet-notify\').style.visibility=\'hidden\';" href="#cachet-notify-close">x</a></span>';
            if (nbSticked == 1) {
                contenu += ' <div id="cachet-notify" style="'+config.divStyle+'">'+closeBoutton+config.txt_1incident;
            } else if (nbSticked >= 2) {
                contenu += ' <div id="cachet-notify" style="'+config.divStyle+'">'+closeBoutton+config.txt_Xincidents;
            }
            // Liste des incidents
            if (config.liste == true) {
                contenu += '<ul id="liste_incidents" style="'+config.ulStyle+'">';
                for (var i = 0; i < sticked.length; i++) {
                    //console.log(sticked[i].name);
                    contenu += '<li class="incident" style="'+config.liStyle+'"><a style="'+config.aStyle+'" href="' + sticked[i].permalink + '" target="_blank">' + sticked[i].name + '</a></li>';
                }
                contenu += '</ul>';
            }
            contenu += '</div>';
            document.body.innerHTML += contenu;
        }

    }
}

