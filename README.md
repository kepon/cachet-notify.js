# cachet-notify.js

Un petit outil qui permet d'afficher sur n'importe quel site par un appel javascript les incidents [cachet](http://cachethq.io/) déclaré en "sticked" 

![](example/ScreenshotExample.png)

## Installer

Installer les dépendances :

```bash
apt install php-curl
```

Déposer le dossier *cachet-notify.js* dans un répertoire accessible en HTTP

```bash
git clone https://framagit.org/kepon/cachet-notify.js.git
```

## Configuration

La configuration se trouve dans le fichier config.json, ce qu'il faut modifier à minima :

```
{
 "urlCachet": "https://status.zici.fr/",   # URL de votre instance cachet
[...]
 "cacheTimeExpire": 600,                   # Temps (en seconde) pour le cache
 "cacheFile": "/var/www/cachet/public/cachet-notify.js/cache/status.cache",          # Localisation du fichier cache (doit être accessible en écriture par le serveur)
}
```

## Incrustation

Sur le site où vous souhaitez voir la notification vous n'avez qu'à incruster le code suivant en pied de page par exemple : 

```html
<script id="cachet-notify.js" src="https://votredomaine.fr/cachet-notify.js/"></script>
```